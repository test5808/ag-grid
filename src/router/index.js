import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import { Grid, Models } from '@/pages'

export const constantRoutes = [
  {
    path: '/',
    component: Grid,
  },
  {
    path: '/models',
    component: Models,
  },
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes,
})

const router = createRouter()

export default router
