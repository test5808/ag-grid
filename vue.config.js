const path = require('path')

function resolve (dir) {
  return path.join(__dirname, '..', dir)
}

module.exports = {
  lintOnSave: true,
  publicPath: './',
  outputDir: './dist',
  devServer: {},
  productionSourceMap: process.env.NODE_ENV !== 'production',
  configureWebpack: {
    resolve: {
      alias: {
        '@css': resolve(`src/assets/css`),
        '@img': resolve(`src/assets/images`)
      }
    },
  },
  css: {
    loaderOptions: {
      scss: {
        data: '@import "@/assets/css/variables.scss";',
      },
    },
  },
  chainWebpack: config => {
  }
};